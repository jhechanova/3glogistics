package com.object;

public class InventoryListObj {
	private String itemDescription;
	private String itemCode;
	private String pieces;
	private String unitWeight;
	private String weight;
	
	private long itemId;
	private long unitId;
	private String unitShort;
	
	public InventoryListObj(String itemCode, String itemDescription, String pieces, String unitWeight, String weight,
			long itemId, long unitId, String unitShort)
	{
		this.itemDescription = itemDescription;
		this.itemCode = itemCode;
		this.pieces = pieces;
		this.weight = weight;
		this.unitWeight = unitWeight;
		
		this.itemId = itemId;
		this.unitId = unitId;
		this.unitShort = unitShort;
	}
	
	public void setItemDescription(String itemDescription)
	{
		this.itemDescription = itemDescription;
	}
	
	public void setItemCode(String itemCode)
	{
		this.itemCode = itemCode;
	}
	
	public void setPieces(String pieces)
	{
		this.pieces = pieces;
	}
	
	public void setUnitWeight(String unitWeight)
	{
		this.unitWeight = unitWeight;
	}
	
	public void setWeight(String weight)
	{
		this.weight = weight;
	}
	
	public void setItemId(long itemId)
	{
		this.itemId = itemId;
	}
	
	public void setUnitId(long unitId)
	{
		this.unitId = unitId;
	}
	
	public void setUnitShort(String unitShort)
	{
		this.unitShort = unitShort;
	}
	
	public String getItemDescription()
	{
		return this.itemDescription;
	}
	
	public String getItemCode()
	{
		return this.itemCode;
	}
	
	public String getPieces()
	{
		return this.pieces;
	}
	
	public String getWeight()
	{
		return this.weight;
	}
	
	public String getUnitWeight()
	{
		return this.unitWeight;
	}
	
	public long getItemId()
	{
		return this.itemId;
	}
	
	public long getUnitId()
	{
		return this.unitId;
	}
	
	public String getUnitShort()
	{
		return this.unitShort;
	}
}
