package com.itemscanner;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;

import com.misc.AlertDialogManager;
import com.misc.CommonValues;
import com.misc.JSONParser;
import com.misc.ObjectElements;
import com.misc.ProgressDialogManager;
import com.object.InventoryListObj;

public class InventoryDisplayActivity extends Activity {

	private ProgressDialogManager progressDialog = null;
	private AlertDialogManager alertDialog = null;
	
	private Context thisContext;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_inventory_display);
		
		ObjectElements.setupInventoryActivityElement(this);
		
		thisContext = this;
		
		progressDialog = new ProgressDialogManager(this);
        alertDialog = new AlertDialogManager(this);
        
		addButtonEventListener();
	}

	public void addButtonEventListener() {   	
    	ObjectElements.invScanButton.setOnClickListener(new OnClickListener() {

			public void onClick(View view) {
				if(ObjectElements.invPalletField.getText().length() == 0)
		    	{
			    	alertDialog.fail("Error", "Pallet code field must not be empty.");
			    	alertDialog.show();
				} else {
					new GetInventoryItem().execute(ObjectElements.invPalletField.getText().toString());
				}
			}
    		
    	});
    }
	
	class GetInventoryItem extends AsyncTask<String, Void, String> {
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			progressDialog.create("Getting inventory item(s). Please wait...");
			progressDialog.show();
		}

		protected String doInBackground(String... args) {
			CommonValues.params.clear();
			
			CommonValues.params.add(new BasicNameValuePair("tpl", "api"));
			CommonValues.params.add(new BasicNameValuePair("tph", "getInventoryItem"));
			CommonValues.params.add(new BasicNameValuePair("pallet_id", args[0]));
			
			JSONParser jsonParser = new JSONParser();
			String jsonString = jsonParser.makeHttpRequest(CommonValues.apiUrl+"/"+CommonValues.session, 
					CommonValues.params);
			
			Log.d("Create Response:", jsonString);
			
			return jsonString;
		}

		protected void onPostExecute(String result) {				
			JSONArray moduleArray = null;
			
			// {"item_code":"{.'item_code'}", "item_desc":"{.'item_desc'}", "uom_name":"{.'uom_name'}", "uom":"{.uom}"
			// , "weight":"{.weight}", "pcs":"{.pcs}", "item_id":"{.item_id}"}
			
			try {	
				JSONObject jsonObj = new JSONObject(result);
				moduleArray = jsonObj.getJSONArray("inventory");
				
				for (int i = 0; i < moduleArray.length(); i++)
				{
					JSONObject moduleField = moduleArray.getJSONObject(i);
					
					CommonValues.inventoryList.add(new InventoryListObj(moduleField.getString("item_code"), 
							moduleField.getString("item_desc"), moduleField.getString("pcs"), moduleField.getString("uom_name"),
							moduleField.getString("weight"), moduleField.getLong("item_id"), moduleField.getLong("uom"),
							moduleField.getString("uom_short")));
				}
				
			} catch (JSONException e) {
				e.printStackTrace();
			}
			
			progressDialog.dismiss();
			
			Intent inventoryListIntent = new Intent(thisContext, InventoryListsActivity.class);
			startActivity(inventoryListIntent);
		}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.inventory_display, menu);
		return true;
	}

}
