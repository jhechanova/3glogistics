package com.itemscanner;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;

import com.itemscanner.InventoryDisplayActivity.GetInventoryItem;
import com.misc.AlertDialogManager;
import com.misc.CommonValues;
import com.misc.JSONParser;
import com.misc.ObjectElements;
import com.misc.ProgressDialogManager;
import com.object.InventoryListObj;

public class PutAwayDisplayActivity extends Activity {

	/**
	 * -- AL010101
	 * -- [room][side][level][rack][depth]
	 * -- [A,B,C..Z][L,R,A][nn][nn][nn]
	 */
	
	private Context thisContext;
	
	private ProgressDialogManager progressDialog = null;
	private AlertDialogManager alertDialog = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_put_away_display);
		
		thisContext = this;
		
		ObjectElements.setupPutAwayActivityElement(this);
		
		progressDialog = new ProgressDialogManager(this);
        alertDialog = new AlertDialogManager(this);
        
		addButtonEventListener();
		
	}
	
	public void addButtonEventListener() {   	
    	ObjectElements.putAwayScanButton.setOnClickListener(new OnClickListener() {

			public void onClick(View view) {
				if(ObjectElements.putAwayPalletField.getText().length() == 0
						|| ObjectElements.putAwayLocationField.getText().length() == 0)
		    	{
			    	alertDialog.fail("Error", "Pallet code or location field must not be empty.");
			    	alertDialog.show();
		    	} else if (ObjectElements.putAwayLocationField.getText().length() != 8) {
		    		alertDialog.fail("Error",  "Location field format is not valid. It must be [room][side][level][rack][depth] e.g. [A,B,C..Z][L,R,A][nn][nn][nn]");
					alertDialog.show();
				} else {
					new UpdatePalletLocation().execute(ObjectElements.putAwayPalletField.getText().toString(),
							ObjectElements.putAwayLocationField.getText().toString());
				}
			}
    		
    	});
    }
	
	class UpdatePalletLocation extends AsyncTask<String, Void, String> {
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			progressDialog.create("Updating pallet location. Please wait...");
			progressDialog.show();
		}

		protected String doInBackground(String... args) {
			CommonValues.params.clear();
			
			CommonValues.params.add(new BasicNameValuePair("tpl", "api"));
			CommonValues.params.add(new BasicNameValuePair("tph", "updatePalletLocation"));
			CommonValues.params.add(new BasicNameValuePair("pallet_id", args[0]));
			
			String room = args[1].substring(0,1);
			String side = args[1].substring(1,2);
			String level = args[1].substring(2,4);
			String rack = args[1].substring(4,6);
			String depth = args[1].substring(6,8);
			
			CommonValues.params.add(new BasicNameValuePair("room", room));
			CommonValues.params.add(new BasicNameValuePair("level", level));
			CommonValues.params.add(new BasicNameValuePair("rack", rack));
			CommonValues.params.add(new BasicNameValuePair("depth", depth));
			
			JSONParser jsonParser = new JSONParser();
			String jsonString = jsonParser.makeHttpRequest(CommonValues.apiUrl+"/"+CommonValues.session, 
					CommonValues.params);
			
			Log.d("Create Response:", jsonString);
			
			return jsonString;
		}

		protected void onPostExecute(String result) {				
			JSONArray moduleArray = null;
			
			try {	
				JSONObject jsonObj = new JSONObject(result);
				moduleArray = jsonObj.getJSONArray("status");
				
				JSONObject moduleField = moduleArray.getJSONObject(0);
				
				if (moduleField.getInt("e") == -1)
					alertDialog.fail("Error", moduleField.getString("msg"));
				else
					alertDialog.success("Success", moduleField.getString("msg"));
				
				alertDialog.show();
				
			} catch (JSONException e) {
				e.printStackTrace();
			}
			
			progressDialog.dismiss();
//			
//			Intent inventoryListIntent = new Intent(thisContext, InventoryListsActivity.class);
//			startActivity(inventoryListIntent);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.put_away_display, menu);
		return true;
	}

}
