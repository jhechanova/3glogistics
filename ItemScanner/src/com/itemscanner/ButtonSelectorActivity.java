package com.itemscanner;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;

import com.misc.ObjectElements;

public class ButtonSelectorActivity extends Activity {

	private Context thisContext;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_button_selector);
		
		ObjectElements.setupButtonSelectorActivityElement(this);
		
		thisContext = this;
		
		addButtonEventListener();
	}

	public void addButtonEventListener() {   	
    	ObjectElements.inventoryButton.setOnClickListener(new OnClickListener() {

			public void onClick(View view) {
				Intent inventoryActivityIntent = new Intent(thisContext, InventoryDisplayActivity.class);
				startActivity(inventoryActivityIntent);
			}
    		
    	});
    	
    	ObjectElements.putAwayButton.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				Intent putAwayActivityIntent = new Intent(thisContext, PutAwayDisplayActivity.class);
				startActivity(putAwayActivityIntent);
			}
		});
    	
    	ObjectElements.dispatchButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent dispatchActivityIntent = new Intent(thisContext, DispatchDisplayActivity.class);
				startActivity(dispatchActivityIntent);
			}
		});
    }
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.button_selector, menu);
		return true;
	}

}
