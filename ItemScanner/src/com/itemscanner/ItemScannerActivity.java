package com.itemscanner;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.widget.Toast;

import com.misc.AlertDialogManager;
import com.misc.CommonValues;
import com.misc.JSONParser;
import com.misc.ObjectElements;
import com.misc.ProgressDialogManager;

public class ItemScannerActivity extends Activity {
	
	private Context thisContext;
	
	private ProgressDialogManager progressDialog = null;
	private AlertDialogManager alertDialog = null;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_scanner);
        
        ObjectElements.setupItemScannerActivityElement(this);        
        
        thisContext = this;
        
        progressDialog = new ProgressDialogManager(this);
        alertDialog = new AlertDialogManager(this);
        
        /** adding EventListener to elements **/
        addTextFieldEventListener();
        addButtonEventListener();
       /** end **/
    }

    private void addTextFieldEventListener() {   	
    	ObjectElements.logInUsername.setOnKeyListener(new OnKeyListener() {
    		
    		public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
    			if(keyEvent.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
    				checkLogin();
    				return true;
    			}
    			
    			return false;
    		}
    		
    	});
    	
    	ObjectElements.logInPassword.setOnKeyListener(new OnKeyListener() {
			
			public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
				if(keyEvent.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
    				checkLogin();
    				return true;
    			}
    			
    			return false;
			}
		});
    	
    }
    
    public void addButtonEventListener() {   	
    	ObjectElements.logInButton.setOnClickListener(new OnClickListener() {

			public void onClick(View view) {
				checkLogin();
			}
    		
    	});
    	
    	ObjectElements.clearButton.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				clearFields();
			}
		});
    }
    
    private void checkLogin() {
    	   	
    	if(ObjectElements.logInUsername.getText().length() == 0 
    			|| ObjectElements.logInPassword.getText().length() == 0)
    	{
	    	alertDialog.fail("Error", "Username and password fields must not be empty.");
	    	alertDialog.show();
		} else {
			try
	    	{
		    	Process p1 = java.lang.Runtime.getRuntime().exec("ping -c 1 " + CommonValues.ipAddr);
		    	int returnVal = p1.waitFor();
		    	boolean reachable = (returnVal==0);
		    	if (!reachable)
		    	{	    		
		    		alertDialog.fail("Error", "No connection established to " + CommonValues.ipAddr +
		    				".The server is unreachable.");
		    		alertDialog.show();
		    		
		    		return;
		    	}
	    	}
	    	catch(Exception ex)
	    	{
	    		ex.printStackTrace();
	    	}            

		    new LoggingIn().execute(ObjectElements.logInUsername.getText().toString(), 
		    		ObjectElements.logInPassword.getText().toString());
		}  	
    }
    
    class LoggingIn extends AsyncTask<String, Void, String> {
    			
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			
			progressDialog.create("Logging In. Please wait...");
			progressDialog.show();
		}

		protected String doInBackground(String... args) {
			CommonValues.params.clear();
			
			CommonValues.params.add(new BasicNameValuePair("cookie_user", args[0]));
			CommonValues.params.add(new BasicNameValuePair("cookie_pass", args[1]));
			CommonValues.params.add(new BasicNameValuePair("op", "i"));
			
			JSONParser jsonParser = new JSONParser();
			String jsonString = jsonParser.makeHttpRequest(CommonValues.apiUrl, 
					CommonValues.params);
			
			Log.d("Create Response:", jsonString);
			
			return jsonString;
		}

		protected void onPostExecute(String result) {	
			progressDialog.dismiss();
			
			try {				
				final JSONObject jsonObj = new JSONObject(result);
				if (jsonObj.getString("key").equals("invalid user/password"))
				{
		    		alertDialog.fail("Error", "Username or password is invalid.");
			    	alertDialog.show();
				}
				else if (jsonObj.getString("key").equals("expired"))
				{
		    		alertDialog.fail("Error", "Session expired.");
			    	alertDialog.show();
				}
				else
				{
					CommonValues.session = jsonObj.getString("key");
					Toast.makeText(ItemScannerActivity.this, "Success Login", Toast.LENGTH_LONG).show();
					
					Intent logInIntent = new Intent(thisContext, ButtonSelectorActivity.class);
					startActivity(logInIntent); 
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
	}
    
    private void clearFields() {
    	ObjectElements.logInPassword.setText("");
    	ObjectElements.logInUsername.setText("");
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.item_scanner, menu);
        return true;
    }
    
}
