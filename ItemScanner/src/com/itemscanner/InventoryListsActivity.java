package com.itemscanner;

import java.util.ArrayList;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ListView;

import com.list.helper.InventoryListHelper;
import com.misc.CommonValues;

public class InventoryListsActivity extends Activity {

	private InventoryListHelper _inventoryListHelper;
	private ListView _listView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
         
        setContentView(R.layout.activity_inventory_lists);
        
        _listView = (ListView) findViewById(R.id.inventory_list_panel);
		
		if (CommonValues.inventoryList != null)
		{		
			_inventoryListHelper = new InventoryListHelper(this, (ArrayList) CommonValues.inventoryList);
			
			 _listView.setAdapter(_inventoryListHelper);
		}
    }
	
	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		if(_inventoryListHelper != null){
			_inventoryListHelper = new InventoryListHelper(this, (ArrayList) CommonValues.inventoryList);
			
			 _listView.setAdapter(_inventoryListHelper);
		}
		
		super.onResume();
	}
}
