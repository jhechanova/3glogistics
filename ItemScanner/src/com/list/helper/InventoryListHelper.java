package com.list.helper;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.itemscanner.R;
import com.object.InventoryListObj;

public class InventoryListHelper extends BaseAdapter {
	private Activity activity;
    private LayoutInflater inflater;
    private ArrayList<InventoryListObj> data;

    public InventoryListHelper (Activity a, ArrayList<InventoryListObj> d){
        activity = a;
        data = d;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
    	InventoryViewHolder _itemViewHolder;
    	
    	if (convertView == null)
    	{
    		_itemViewHolder = new InventoryViewHolder();
    		convertView = inflater.inflate(R.layout.inventory_list, null);

    		_itemViewHolder.item_code = (TextView)convertView.findViewById(R.id.item_code);
    		_itemViewHolder.item_desc = (TextView)convertView.findViewById(R.id.item_desc);
    		_itemViewHolder.pieces = (TextView)convertView.findViewById(R.id.pieces);
    		_itemViewHolder.weight= (TextView)convertView.findViewById(R.id.weight);
    		
    		convertView.setTag(_itemViewHolder);
    	}
    	else
    	{
    		_itemViewHolder = (InventoryViewHolder) convertView.getTag();
    	}
    	
        final InventoryListObj _inventoryListObj = data.get(position);
        _itemViewHolder.item_code.setText(_inventoryListObj.getItemCode());
        _itemViewHolder.item_desc.setText(_inventoryListObj.getItemDescription());
        _itemViewHolder.pieces.setText(_inventoryListObj.getPieces()+""+_inventoryListObj.getUnitShort());
        _itemViewHolder.weight.setText(_inventoryListObj.getWeight());
        
        return convertView;
    }
    
    public class InventoryViewHolder
    {
    	TextView item_code;
    	TextView item_desc;
    	TextView pieces;
    	TextView weight;
    }
}
