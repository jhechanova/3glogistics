package com.misc;

import android.app.ProgressDialog;
import android.content.Context;

public class ProgressDialogManager {
	
	ProgressDialog progressDialog;
	
	public ProgressDialogManager (Context context) {
		progressDialog = new ProgressDialog(context);
	}
	
	public void create (String progressMessage) {
		progressDialog.setMessage(progressMessage);
		progressDialog.setIndeterminate(false);
		progressDialog.setCancelable(false);
	}
	
	public void show() {
		progressDialog.show();
	}
	
	public void dismiss() {
		progressDialog.cancel();
	}
}
