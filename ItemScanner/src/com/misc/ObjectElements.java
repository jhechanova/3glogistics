package com.misc;

import android.app.Activity;
import android.content.Context;
import android.widget.Button;
import android.widget.EditText;

import com.itemscanner.R;

public class ObjectElements {	
	public static Button inventoryButton;
	public static Button putAwayButton;
	public static Button dispatchButton;
	
	public static EditText logInUsername;
	public static EditText logInPassword;
	public static Button logInButton;
	public static Button clearButton;
	
	public static EditText invPalletField;
	public static Button invScanButton;
	
	public static EditText putAwayPalletField;
	public static EditText putAwayLocationField;
	public static Button putAwayScanButton;
	
	public static void setupItemScannerActivityElement(Context context)
	{
		logInUsername = (EditText) ((Activity) context).findViewById(R.id.logInUsername);
		logInPassword = (EditText) ((Activity) context).findViewById(R.id.logInPassword);
		logInButton = (Button) ((Activity) context).findViewById(R.id.logInBtn);
		clearButton = (Button) ((Activity) context).findViewById(R.id.clearBtn);
	}
	
	public static void setupButtonSelectorActivityElement(Context context)
	{
		inventoryButton = (Button) ((Activity) context).findViewById(R.id.inventory_btn);
		putAwayButton = (Button) ((Activity) context).findViewById(R.id.put_away_btn);
		dispatchButton = (Button) ((Activity) context).findViewById(R.id.dispatch_btn);
	}
	
	public static void setupInventoryActivityElement(Context context)
	{
		invPalletField = (EditText) ((Activity) context).findViewById(R.id.invPalletField);
		invScanButton = (Button) ((Activity) context).findViewById(R.id.invScanButton);
	}
	
	public static void setupPutAwayActivityElement(Context context)
	{
		putAwayPalletField = (EditText) ((Activity) context).findViewById(R.id.putAwayPalletField);
		putAwayLocationField = (EditText) ((Activity) context).findViewById(R.id.putAwayLocationField);
		putAwayScanButton = (Button) ((Activity) context).findViewById(R.id.putAwayScanButton);
	}
}
