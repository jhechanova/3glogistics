package com.misc;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import com.itemscanner.R;

public class AlertDialogManager {
	
	AlertDialog.Builder alertDialog;
	
	public AlertDialogManager(Context context) {
		alertDialog = new AlertDialog.Builder(context);
	}
		
	public void confirm(String dialogTitle, String dialogMessage, 
			DialogInterface.OnClickListener yesCallback, DialogInterface.OnClickListener noCallback)
	{
		create(dialogTitle, dialogMessage, R.drawable.confirm);
		
		alertDialog.setPositiveButton("Yes", yesCallback);
		
		alertDialog.setNegativeButton("No", noCallback);
		
		alertDialog.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				dialog.cancel();
			}
		});
	}
	
	public void success(String dialogTitle, String dialogMessage)
	{
		create(dialogTitle, dialogMessage, R.drawable.success);
		
		alertDialog.setNeutralButton("Ok", new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				dialog.cancel();
			}
		});
	}
	
	public void fail(String dialogTitle, String dialogMessage)
	{
		create(dialogTitle, dialogMessage, R.drawable.fail);
		
		alertDialog.setNeutralButton("Ok", new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				dialog.cancel();
			}
		});
	}
	
	private void create (String dialogTitle, String dialogMessage, int icon)
	{ 
		alertDialog.setTitle(dialogTitle);
		alertDialog.setMessage(dialogMessage);
		alertDialog.setIcon(icon);
	}
	
	public void show () {
		alertDialog.show();
	}
}
